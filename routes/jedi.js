var express = require('express');
const db = require("../models");
const Jedi = db.jedi;

var app = express();

// =================
// List all models
// =================
app.get('/', (req, res, next) => {

    Jedi.findAll()
    .then(data => {
        
        // Successful response
        res.status(200).json({
            ok: true,
            data: data
        });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ha ocurrido un error al intentar obtener los jedi."
      });
    });
})

// =================
// Create new model
// =================
app.post('/', (req, res) => {

    // Get params from request
    const data = {
        names: req.body.nombres
    };

    // Validate if has value
    if(data.names == ''){
        return res.status(422).json({
            ok: false,
            message: 'Ocurrió un problema',
            errors: { 
                nombres: 'El nombre del jedi es requerido' 
            }
        });
    }

    // Search if exist
    Jedi.findOne({
        where: {
            names: data.names
        }
    }).then(user => {
        // Valid if exist
        if(user != null){

            res.status(422).json({
                ok: false,
                message: 'Ocurrió un problema',
                errors: { 
                    nombres: 'El nombre del jedi ya esta registrado' 
                }
            });

        }else{
            
            // Create new Model if dont exist
            Jedi.create(data)
            .then(data => {

                // Successful response
                res.status(201).json({
                    ok: true,
                    data: data
                });
            })
        }

    }).catch(err => {
        res.status(500).send({
            ok: false,
            message:
                err.message || "Un error ocurrió al momento de intentar guardar un nuevo Jedi."
        });
    });

})

module.exports = app;