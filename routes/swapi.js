const express = require('express');
const swapi = require('swapi-node');

var app = express();

// =================
// List all planets
// =================
app.get('/planets', (req, res, next) => {

    // Get planets
    swapi.get('https://swapi.dev/api/planets/').then((result) => {
        
        // Response
        res.status(200).json({
            ok: true,
            message: result
        });
    });
    
})

// =================
// List all films
// =================
app.get('/films', (req, res, next) => {

    // Get planets
    swapi.get('https://swapi.dev/api/films/').then((result) => {
        
        // Response
        res.status(200).json({
            ok: true,
            message: result
        });
    });
    
})

module.exports = app;