module.exports = (sequelize, Sequelize) => {
    const Jedi = sequelize.define("jedis", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      names: {
        type: Sequelize.STRING
      },
      planets: {
        type: Sequelize.STRING
      }
    });
  
    return Jedi;
  };