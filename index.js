const serverless = require('serverless-http');
const express = require('express');
const db = require('./models/index.js');

const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Init express
const app = express()

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "LogRocket Express API with Swagger",
            version: "0.1.0",
            description:
                "This is a simple CRUD API application made with Express and documented with Swagger",
            license: {
                name: "MIT",
                url: "https://spdx.org/licenses/MIT.html",
            },
            contact: {
                name: "LogRocket",
                url: "https://logrocket.com",
                email: "info@email.com",
            },
        },
        servers: [
            {
                url: "http://localhost:3000/dev/",
            },
        ],
    },
    apis: ["./routes/jedi.js"],
};

const specs = swaggerJsdoc(options);
app.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(specs)
);


// Init database models on sequalize
db.sequelize.sync();

// Init Routes
const swapiRoutes = require('./routes/swapi');
const jediRoutes = require('./routes/jedi');

app.use(express.urlencoded({ extended: true }));
app.use(express.json())
app.use('/swapi', swapiRoutes);
app.use('/jedi', jediRoutes);

module.exports.handler = serverless(app);