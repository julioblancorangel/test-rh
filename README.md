# test-rh

## CONFIG DATABASE CONNECTION

Database environment connection is located in config/db.config.js file

## RUN LOCALLY

```
sls offline
```

## BASE URL

```
http://<domain>/<stage>/
```

If you run this locally the base url will have default dev stage and will run on 3000 port

```
http://localhost:3000/dev/
```

## METHODS

# Get Planets on SWAPI

**URL** : `/swapi/planets`

**Method** : `GET`

**Auth required** : NO

**Data constraints**

```json
{
    "count": 60,
    "next": "https://swapi.dev/api/planets/?page=2",
    "previous": null,
    "results": [
        {
            "name": "Tatooine",
            "rotation_period": "23",
            "orbital_period": "304",
            "diameter": "10465",
            "climate": "arid",
            "gravity": "1 standard",
            "terrain": "desert",
            "surface_water": "1",
            "population": "200000",
            "residents": [
                "https://swapi.dev/api/people/1/",
                "https://swapi.dev/api/people/2/",
                "https://swapi.dev/api/people/4/",
                "https://swapi.dev/api/people/6/",
                "https://swapi.dev/api/people/7/",
                "https://swapi.dev/api/people/8/",
                "https://swapi.dev/api/people/9/",
                "https://swapi.dev/api/people/11/",
                "https://swapi.dev/api/people/43/",
                "https://swapi.dev/api/people/62/"
            ],
            "films": [
                "https://swapi.dev/api/films/1/",
                "https://swapi.dev/api/films/3/",
                "https://swapi.dev/api/films/4/",
                "https://swapi.dev/api/films/5/",
                "https://swapi.dev/api/films/6/"
            ],
            "created": "2014-12-09T13:50:49.641000Z",
            "edited": "2014-12-20T20:58:18.411000Z",
            "url": "https://swapi.dev/api/planets/1/"
        }
    ]
}
```

# Get Films on SWAPI

**URL** : `/swapi/films`

**Method** : `GET`

**Auth required** : NO

**Data constraints**

```json
{
    "count": 6,
    "next": null,
    "previous": null,
    "results": [
        {
            "title": "A New Hope",
            "episode_id": 4,
            "opening_crawl": "It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire's\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire's\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....",
            "director": "George Lucas",
            "producer": "Gary Kurtz, Rick McCallum",
            "release_date": "1977-05-25",
            "characters": [
                "https://swapi.dev/api/people/1/"
            ],
            "planets": [
                "https://swapi.dev/api/planets/1/"
            ],
            "starships": [
                "https://swapi.dev/api/starships/2/"
            ],
            "vehicles": [
                "https://swapi.dev/api/vehicles/4/"
            ],
            "species": [
                "https://swapi.dev/api/species/1/"
            ],
            "created": "2014-12-10T14:23:31.880000Z",
            "edited": "2014-12-20T19:49:45.256000Z",
            "url": "https://swapi.dev/api/films/1/"
        }
    ]
}
```

# Create a Jedi

**URL** : `/jedi`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "nombres": "[names in plain text]"
}
```

**Data example**

```json
{
    "nombres": "Cal Loran"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "ok": true,
    "data": {
        "id": 1,
        "names": "Cal Loran",
        "updatedAt": "2021-06-29T03:19:10.066Z",
        "createdAt": "2021-06-29T03:19:10.066Z"
    }
}
```

## Error Response

**Condition** : If 'names' is empty

**Code** : `422 Unprocessable Entity`

**Content** :

```json
{
    "ok": false,
    "message": "Ocurrió un problema",
    "errors": {
        "nombres": "El nombre del jedi es requerido"
    }
}
```

**Condition** : If 'names' already exist

**Code** : `422 Unprocessable Entity`

**Content** :

```json
{
    "ok": false,
    "message": "Ocurrió un problema",
    "errors": {
        "nombres": "El nombre del jedi ya esta registrado"
    }
}
```

# List all Jedis

**URL** : `/jedi`

**Method** : `GET`

**Auth required** : NO

**Data constraints**

```json
{
    "ok": true,
    "data": [
        {
            "id": 1,
            "names": "Cal Loran",
            "planets": null,
            "createdAt": "2021-06-29T02:53:31.000Z",
            "updatedAt": "2021-06-29T02:53:31.000Z"
        }
    ]
}
```